<?php

/**
 * @author Grzegorz Banaś <grzegorz@uniti.pl>
 * @link http://www.uniti.pl
 * @version 1.0
 */

namespace Rest\Authorization;

class Basic extends Authorization {
    
    protected $login, $password;
    
    public function __construct( string $login = null, string $password = null ) {
        $this->login = $login;
        $this->password = $password;
    }
    
    public function inject( $curl ) {
        curl_setopt( $curl, CURLOPT_USERPWD, $this->login . ':' . $this->password );
        return true;
    }
    
}