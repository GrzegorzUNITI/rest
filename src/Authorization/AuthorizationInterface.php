<?php

/**
 * @author Grzegorz Banaś <grzegorz@uniti.pl>
 * @link http://www.uniti.pl
 * @version 1.0
 */

namespace Rest\Authorization;

interface AuthorizationInterface {
    public function inject( $curl );
}