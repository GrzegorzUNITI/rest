<?php

/**
 * @author Grzegorz Banaś <grzegorz@uniti.pl>
 * @link http://www.uniti.pl
 * @version 1.0
 */

namespace Rest\Formats;

interface FormatsInterface {
    public function inject( $data );
    public function header();
}