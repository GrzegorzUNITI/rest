<?php

/**
 * @author Grzegorz Banaś <grzegorz@uniti.pl>
 * @link http://www.uniti.pl
 * @version 1.0
 */

namespace Rest\Formats;

class JSON extends Formats {
    
    public function header() {
        return 'application/json';
    }
    
    public function inject( $data ) {
        return json_decode( $data, true );
    }
    
}