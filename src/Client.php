<?php

/**
 * @author Grzegorz Banaś <grzegorz@uniti.pl>
 * @link http://www.uniti.pl
 * @version 1.0
 */

namespace Rest;

use Exceptions\RestException;

class Client extends Base {
    
    public function __construct() {
        
        if ( ! extension_loaded( self::CURL ) ) {
            throw new RestException( 'Before using Rest API Client, install cURL PHP lib.' );
        }
        
        $this->curl = curl_init();
        
        curl_setopt( $this->curl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $this->curl, CURLOPT_FOLLOWLOCATION, 1 );
        curl_setopt( $this->curl, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt( $this->curl, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $this->curl, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT );
        curl_setopt( $this->curl, CURLOPT_MAXREDIRS, self::MAX_REDIRECTIONS );
    }
    
    public function execute() {
        
        curl_setopt( $this->curl, CURLOPT_URL, $this->url );
        curl_setopt( $this->curl, CURLOPT_CUSTOMREQUEST, $this->method );
        
        if ( $this->authorization && is_object( $this->authorization ) ) {
            if ( ! $this->authorization->inject( $this->curl ) ) {
                throw new RestException( 'Can not inject authorization to cURL setup' );
            }
        }
        
        if ( $this->data && count( $this->data ) > 0 ) {
            $this->assignData( $this->data );
        }
        
        if ( $this->format && is_object( $this->format ) ) {
            $this->headers[] = 'Content-Type: ' . $this->format->header();
        }
        
        if ( $this->headers && @count( $this->headers ) ) {
            curl_setopt( $this->curl, CURLOPT_HTTPHEADER, $this->headers );
        }

        if ( ! ( $this->response = curl_exec( $this->curl ) ) ) {
            throw new RestException( 'cURL Error: ' . curl_error( $this->curl ) );
        }

        $this->status = (int) curl_getinfo( $this->curl, CURLINFO_HTTP_CODE );
        
        if ( $this->format && is_object( $this->format ) ) {
            if ( ! ( $this->response_formatted = $this->format->inject( $this->response ) ) ) {
                throw new RestException( 'Can not format response by injecting formatter' );
            }
        } else {
            $this->response_formatted = $this->response;
        }

        if ( $this->status >= 200 && $this->status < 300 ) {
            if ( $this->success && is_callable( $this->success ) ) {
                call_user_func_array( $this->success, [
                    'body'      => $this->response_formatted,
                    'raw'       => $this->response,
                    'status'    => $this->status
                ]);
            } else {
                return $this->response;
            }
        } else {
            if ( $this->catch && is_callable( $this->catch ) ) {
                call_user_func_array( $this->catch, [
                    'body'      => $this->response_formatted,
                    'raw'       => $this->response,
                    'status'    => $this->status
                ]);
            } else {
                return $this->response;
            }
        }
    }
    
    protected function assignData( $data ) {
        $data_json = json_encode( $data );
        curl_setopt( $this->curl, CURLOPT_POST, true );
        curl_setopt( $this->curl, CURLOPT_POSTFIELDS, $data_json );
        $this->headers[] = 'Content-Length: ' . strlen( $data_json );
    }
    
    public function __destruct() {
        curl_close( $this->curl );
    }

}