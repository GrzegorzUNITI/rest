<?php

/**
 * @author Grzegorz Banaś <grzegorz@uniti.pl>
 * @link http://www.uniti.pl
 * @version 1.0
 */

/**
 * TEST
 */

namespace Rest;

abstract class Base implements BaseInterface {
    
    const CURL = 'curl';
    const TIMEOUT = 20;
    const MAX_REDIRECTIONS = 10;
    
    const GET = 'GET';
    const POST = 'POST';
    // {...}
    
    public      $response,
                $response_formatted,
                $status;
    
    protected   $curl,
                $method,
                $url,
                $authorization,
                $format,
                $success,
                $catch;
    
    protected $headers = [];
    protected $data = [];
    
    /*
    public function __construct( string $method = 'GET', string $url = null ) {
        $this->method = $method;
        $this->url = $url;
        $this->initialize();
    }
    */
    
    public function url( string $url ) {
        $this->url = $url;
        return $this;
    }
    
    public function method( string $method ) {
        $this->method = $method;
        return $this;
    }
    
    public function auth( Authorization\Authorization $authorization ) {
        $this->authorization = $authorization;
        return $this;
    }
    
    public function format( Formats\Formats $formatter ) {
        $this->format = $formatter;
        return $this;
    }
    
    public function data( array $data ) {
        $this->data = $data;
        return $this;
    }
    
    public function success( $function ) { // function => object
        $this->success = $function;
        return $this;
    }
    
    public function catch( $function ) { // function => object
        $this->catch = $function;
        return $this;
    }
    
    public function addHeader( string $header ) {
        $this->headers[] = $header;
    }
}
